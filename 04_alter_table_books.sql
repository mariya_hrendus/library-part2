use M_Hrendus_Library
go
alter table Books
add 
title varchar(30) not null default 'Title',
edition int not null default 1 check(edition >= 1),
published date null,
issue int null
go