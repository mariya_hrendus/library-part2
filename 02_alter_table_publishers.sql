use M_Hrendus_Library
go
alter table Publishers
add
created date not null default '1900-01-01',
country varchar(30) not null default 'USA',
city varchar(30) not null default 'NY',
book_amount int   not null default 0 check (book_amount >= 0),
issue_amount int not null default 0 check (issue_amount >= 0),
total_edition int not null default 0 check (total_edition >= 0)
go